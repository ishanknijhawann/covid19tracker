---


---

<h1 id="covid19-tracker">Covid19 Tracker</h1>
<p>This app gives the data of number of patients affected by Covid19 Virus till date in India. It also gives statewise distribution of the count as shown in the image. This app also notifies the user every hour regarding the count status so that the user doesen’t have to open the app every time to check the status.</p>
<p>Download the app from <a href="https://drive.google.com/open?id=1l5Jk8C68UX739b4H2vY-ph99Wm3Mjt0U">here</a></p>

<table>
<thead>
<tr>
<th>India count</th>
<th>Global count</th>
</tr>
</thead>
<tbody>
<tr>
<td><img src="https://user-images.githubusercontent.com/45118110/80280012-de813600-871e-11ea-9130-7898c799660b.png" width="310" height="550"></td>
<td><img src="https://user-images.githubusercontent.com/45118110/80280012-de813600-871e-11ea-9130-7898c799660b.png" width="310" height="550"></td>
</tr>
</tbody>
</table>
<table>
<thead>
<tr>
<th>Notification</th>
<th>Search places</th>
</tr>
</thead>
<tbody>
<tr>
<td><img src="https://user-images.githubusercontent.com/45118110/80280065-3cae1900-871f-11ea-8d36-f3a23d2188bf.png" width="310" height="550"></td>
<td><img src="https://user-images.githubusercontent.com/45118110/80280330-083b5c80-8721-11ea-8685-1e9cb912db57.png" width="310" height="550"></td>
</tr>
</tbody>
</table>
